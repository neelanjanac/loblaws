package com.loblaws.kotlinnews.extensions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup?.inflate(
    layoutId: Int,
    attachToRoot: Boolean = false,
    context: Context? = null
): View {
    return LayoutInflater.from(this?.context ?: context).inflate(layoutId, this, attachToRoot)
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}




