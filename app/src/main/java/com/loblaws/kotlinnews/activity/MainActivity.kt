package com.loblaws.kotlinnews.activity

import android.os.Bundle
import com.loblaws.kotlinnews.R
import com.loblaws.kotlinnews.extensions.showFragment
import com.loblaws.kotlinnews.ui.base.BaseActivity
import com.loblaws.kotlinnews.ui.view.NewsListFragment

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.showFragment(
                NewsListFragment(),
                R.id.fragment_container, true
        )
    }
}