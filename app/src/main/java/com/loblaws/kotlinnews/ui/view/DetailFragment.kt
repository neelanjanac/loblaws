package com.loblaws.kotlinnews.ui.view

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.webkit.URLUtil
import com.bumptech.glide.Glide
import com.loblaws.data.model.ChildData
import com.loblaws.kotlinnews.R
import com.loblaws.kotlinnews.extensions.gone
import com.loblaws.kotlinnews.extensions.visible
import com.loblaws.kotlinnews.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment : BaseFragment(), View.OnClickListener {

    private var model: ChildData? = null

    companion object {
        var MODEL_DATA = "data"
        fun newInstance(bundle: Bundle?): DetailFragment {
            val fragment = DetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = arguments?.getParcelable(MODEL_DATA)
    }

    override fun getLayout(): Int {
        return R.layout.fragment_detail
    }

    override fun viewReady() {
        back?.setOnClickListener(this)

        title?.text = model?.data?.title

        if (TextUtils.isEmpty(model?.data?.url)) {
            url?.gone()
        } else {
            url?.visible()
            url?.text = model?.data?.url
            url?.paintFlags = Paint.UNDERLINE_TEXT_FLAG
            url?.setOnClickListener(this)
        }

        // inflate selftext if any
        if (TextUtils.isEmpty(model?.data?.selftext)) {
            detail_container?.gone()
        } else {
            url?.gone()
            detail_container?.visible()
            detailed_text?.text = model?.data?.selftext
        }


        // attach thumbnail url to imageview
        if (URLUtil.isValidUrl(model?.data?.thumbnail)) {
            var ratio =
                model?.data?.thumbnail_height?.div(model?.data?.thumbnail_width ?: 0.0f)

            image?.setAspectRatio(ratio ?: 0.0f)

            Glide.with(context!!)
                .load(model?.data?.thumbnail)
                .into(image)
            image?.visible()

        } else {
            image?.gone()
        }
    }

    override fun onClick(p0: View?) {
        if (p0?.id == R.id.back) {
            onBackPressed()
        } else if (p0?.id == R.id.url) {
            val openUrl = Intent(Intent.ACTION_VIEW, Uri.parse(model?.data?.url))
            startActivity(openUrl)
        }
    }


}