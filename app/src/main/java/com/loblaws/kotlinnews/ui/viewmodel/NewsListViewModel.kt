package com.loblaws.kotlinnews.ui.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.loblaws.data.model.NewsModel
import com.loblaws.data.repository.Repository
import com.loblaws.kotlinnews.ui.base.BaseViewModel

class NewsListViewModel : BaseViewModel() {
    var data = MediatorLiveData<NewsModel>()
    private var repository: Repository? = Repository.instance()

    init {

        repository?.mResponse?.let { it ->
            data?.addSource(it, Observer {
                data.postValue(it)
            })
        }

    }

    fun getData() {
        repository?.makeAPICall()
    }

}