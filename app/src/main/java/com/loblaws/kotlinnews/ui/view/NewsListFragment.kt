package com.loblaws.kotlinnews.ui.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.loblaws.data.model.ChildData
import com.loblaws.kotlinnews.R
import com.loblaws.kotlinnews.extensions.*
import com.loblaws.kotlinnews.ui.adapter.NewsListAdapter
import com.loblaws.kotlinnews.ui.base.BaseFragment
import com.loblaws.kotlinnews.ui.viewmodel.NewsListViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class NewsListFragment : BaseFragment(), View.OnClickListener {

    private var listAdapter: NewsListAdapter? = null

    private val viewModel: NewsListViewModel by lazy {
        ViewModelProvider(this).get(NewsListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listAdapter = NewsListAdapter()
        listAdapter?.onClicked = this::onClick
        viewModel.getData()
    }


    override fun viewReady() {

        subscribeToViewModel()
        recyclerView?.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView?.adapter = listAdapter

        if (viewModel?.data?.value == null) {
            showLoading(progressBar = loader)
        }

    }


    override fun getLayout(): Int {
        return R.layout.fragment_home
    }

    private fun subscribeToViewModel() {
        viewModel.data.subscribe(viewLifecycleOwner) {
            hideLoading(progressBar = loader)
            if (it != null) {
                listAdapter?.setData(it?.data?.children)
            } else {
                showError("Something went wrong!", view!!)
            }

        }
    }

    override fun onClick(p0: View?) {
        var id = p0?.id
        when (id) {
            R.id.list_item -> {
                var bundle = Bundle()
                bundle.putParcelable(DetailFragment.MODEL_DATA, p0?.tag as ChildData)
                var fragment = DetailFragment.newInstance(bundle)
                activity?.pushFragment(fragment)
            }

        }
    }

}