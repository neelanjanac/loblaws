package com.loblaws.kotlinnews.ui.base

import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.loblaws.kotlinnews.extensions.*

abstract class BaseActivity : AppCompatActivity() {


    fun showLoading(progressBar: ProgressBar) = progressBar?.visible()

    fun hideLoading(progressBar: ProgressBar) = progressBar?.gone()

    fun showError(errorMessage: String?, rootView: View) =
        snackBar(errorMessage ?: "Error", rootView)

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount <= 1) finish() else goBack()
    }
}
