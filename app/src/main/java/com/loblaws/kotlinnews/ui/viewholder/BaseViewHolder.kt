package com.loblaws.kotlinnews.ui.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.loblaws.kotlinnews.extensions.inflate

abstract class BaseViewHolder(open var layout: Int, open var parent: ViewGroup?) :
    RecyclerView.ViewHolder(parent.inflate(layout)) {
    open var onClicked: ((view: View) -> Any)? = null

}