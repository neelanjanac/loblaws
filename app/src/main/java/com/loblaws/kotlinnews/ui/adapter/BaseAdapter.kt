package com.loblaws.kotlinnews.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.loblaws.data.model.BaseModel
import com.loblaws.kotlinnews.ui.viewholder.BaseViewHolder

abstract class BaseAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    var dataList: ArrayList<out BaseModel>? = null
    open var onClicked: ((view: View) -> Any)? = null

    abstract fun onBindView(holder: BaseViewHolder, position: Int)

    fun setData(list: ArrayList<out BaseModel>?) {
        this.dataList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    final override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        onBindView(holder, position)
    }

}
