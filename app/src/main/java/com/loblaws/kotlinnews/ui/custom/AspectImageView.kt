package com.loblaws.kotlinnews.ui.custom

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.IntDef
import androidx.appcompat.widget.AppCompatImageView
import com.loblaws.kotlinnews.R
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy


class AspectRatioImageView : AppCompatImageView {
    private var aspect = 0
    private var aspectRatio = 0f

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context!!, attrs, defStyleAttr) {
        init(attrs)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val height = measuredHeight
        val width = measuredWidth
        when (aspect) {
            AUTO -> if (height > width) {
                if (width == 0) {
                    return
                }
                aspect = WIDTH
                aspectRatio = Math.round(height.toDouble() / width).toFloat()
                setMeasuredDimensionByHeight(height)
            } else {
                if (height == 0) {
                    return
                }
                aspect = HEIGHT
                aspectRatio = Math.round(width.toDouble() / height).toFloat()
                setMeasuredDimensionByWidth(width)
            }
            WIDTH -> setMeasuredDimensionByHeight(height)
            HEIGHT -> setMeasuredDimensionByWidth(width)
            else -> setMeasuredDimensionByWidth(width)
        }
    }

    private fun init(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioView)
        try {
            aspect = a.getInt(
                R.styleable.AspectRatioView_aspect,
                HEIGHT
            )
            aspectRatio = a.getFloat(
                R.styleable.AspectRatioView_ratio,
                DEFAULT_RATIO.toFloat()
            )
        } finally {
            a.recycle()
        }
    }

    private fun setMeasuredDimensionByWidth(width: Int) {
        setMeasuredDimension(width, (width * aspectRatio).toInt())
    }

    private fun setMeasuredDimensionByHeight(height: Int) {
        setMeasuredDimension((height * aspectRatio).toInt(), height)
    }

    fun getAspectRatio(): Double {
        return aspectRatio.toDouble()
    }

    fun setAspectRatio(ratio: Float) {
        aspectRatio = ratio
        requestLayout()
    }

    @Aspect
    fun getAspect(): Int {
        return aspect
    }

    fun setAspect(aspect: Int) {
        this.aspect = aspect
        requestLayout()
    }

    @IntDef(WIDTH, HEIGHT)
    @Retention(RetentionPolicy.SOURCE)
    annotation class Aspect
    companion object {
        private const val DEFAULT_RATIO = 1
        private const val WIDTH = 0
        private const val HEIGHT = 1
        private const val AUTO = 2
    }
}