package com.loblaws.kotlinnews.ui.adapter

import android.view.ViewGroup
import com.loblaws.data.model.ChildData
import com.loblaws.kotlinnews.R
import com.loblaws.kotlinnews.ui.viewholder.BaseViewHolder
import com.loblaws.kotlinnews.ui.viewholder.ListViewHolder

class NewsListAdapter : BaseAdapter() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val holder = ListViewHolder(R.layout.layout_news_item, parent)
        holder?.onClicked = onClicked
        return holder

    }

    override fun onBindView(holder: BaseViewHolder, position: Int) {
        (holder as? ListViewHolder?)?.bindData(dataList?.get(position) as ChildData)
    }
}