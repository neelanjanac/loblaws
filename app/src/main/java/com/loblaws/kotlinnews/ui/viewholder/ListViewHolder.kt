package com.loblaws.kotlinnews.ui.viewholder

import android.view.ViewGroup
import android.webkit.URLUtil
import com.bumptech.glide.Glide
import com.loblaws.data.model.ChildData
import com.loblaws.kotlinnews.extensions.gone
import com.loblaws.kotlinnews.extensions.visible
import kotlinx.android.synthetic.main.layout_news_item.view.*

class ListViewHolder(override var layout: Int, parent: ViewGroup?) :
    BaseViewHolder(layout, parent) {

    fun bindData(model: ChildData?) {

        if (URLUtil.isValidUrl(model?.data?.thumbnail)) {

            var ratio =
                model?.data?.thumbnail_height?.div(model?.data?.thumbnail_width ?: 0.0f)

            itemView?.thumbnail?.setAspectRatio(ratio ?: 0.0f)

            Glide.with(itemView?.context)
                .load(model?.data?.thumbnail)
                .into(itemView?.thumbnail)
            itemView?.thumbnail?.visible()
        } else {
            itemView?.thumbnail?.gone()
        }


        itemView?.title?.text = model?.data?.title

        itemView?.setOnClickListener {
            it?.tag = model
            onClicked?.invoke(it)
        }

    }

}