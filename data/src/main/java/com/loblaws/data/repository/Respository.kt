package com.loblaws.data.repository

import androidx.lifecycle.MutableLiveData
import com.loblaws.data.model.NewsModel
import com.loblaws.data.networking.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    var mResponse = MutableLiveData<NewsModel>()
    private lateinit var service: RetrofitService

    companion object {
        private var instance: Repository? = null
        fun instance(): Repository? {
            if (instance == null) {
                instance = synchronized(this) { Repository() }
            }
            return instance
        }
    }

    fun makeAPICall() {
        service = RetrofitService.create()
        service.getMovies()?.enqueue(object : Callback<NewsModel> {
            override fun onResponse(
                call: Call<NewsModel>?,
                response: Response<NewsModel>?
            ) {
                mResponse?.postValue(response?.body())
            }

            override fun onFailure(call: Call<NewsModel>?, t: Throwable?) {
                mResponse?.postValue(null)
            }
        })

    }


}


