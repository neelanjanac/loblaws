package com.loblaws.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

interface BaseModel

@Parcelize
data class NewsModel(
    var data: Data? = null
) : BaseModel, Parcelable


@Parcelize
data class Data(
    var children: ArrayList<ChildData>? = null
) : Parcelable

@Parcelize
data class ChildData(
    var data: NewsItem? = null
) : BaseModel,Parcelable


@Parcelize
data class NewsItem(
    var title: String? = null,
    var selftext: String? = null,
    var thumbnail: String? = null,
    var thumbnail_height: Float? = null,
    var thumbnail_width: Float? = null,
    var url: String? = null
) : Parcelable