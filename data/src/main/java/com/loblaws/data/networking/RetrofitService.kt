package com.loblaws.data.networking

import com.loblaws.data.model.NewsModel
import com.loblaws.data.ApiConstants
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface RetrofitService {

    @GET("r/kotlin/.json")
    fun getMovies(): Call<NewsModel>

    companion object {
        fun create(): RetrofitService {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiConstants.BASE_URL)
                .build()
            return retrofit.create(RetrofitService::class.java)

        }


    }


}